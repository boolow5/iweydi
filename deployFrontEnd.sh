export OWNER=mahdi
export BASE_DIR=$PWD
export SOURCE_DIR=$BASE_DIR/public/iweydi/dist
export SERVER=iweydi.com
export DESTINATION_DIR=/webapps/iweydi/public

echo "deploying ${DESTINATION_DIR} to ${OWNER}@${SERVER}"
rsync -auv -e ssh --progress $SOURCE_DIR/* $OWNER@$SERVER:$DESTINATION_DIR
