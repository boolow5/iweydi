OSTYPE = ${shell echo $$OSTYPE}
APP_NAME := iweydi
PKGNAME := iweydi.tar.gz
ACTUAL := ${shell pwd}
VERSION_NUMBER := 0.0.2
VERSION := '${VERSION_NUMBER}\:$(shell date -u +%Y%m%d.%H%M%S)\($(shell git rev-parse --short HEAD)\)'

export OSTYPE;
export ACTUAL;
export PKGNAME;
export VERSION;
export FORKS;

build: linux-build package

linux-build:
	GOOS=linux GOARCH=amd64 go build -v -ldflags "-X bitbucket.org/boolow5/iweydi/models.Version=${VERSION}" -o bin/iweydi

clean:
	rm -r bin/*

init:
	mkdir -p bin log
	go get -v -u ./..

package:
	@mv bin old_bin;
	@mkdir -p bin;
	@cp old_bin/iweydi bin/;
	@cp -r data bin/;

	@tar -zcf ${PKGNAME} bin/;
	@rm -r bin;
	@mv old_bin bin;
	@echo "Packaging of ${PKGNAME} is complete. run make deploy to send ${APP_NAME} to server."

deploy:
	@bash deploy.sh

publish:
	@bash buildFrondEnd.sh
	@bash deployFrontEnd.sh
