package routers

import (
	"bitbucket.org/boolow5/iweydi/controllers"
	"bitbucket.org/boolow5/iweydi/middlewares"
	"github.com/gin-gonic/gin"
)

func Init() *gin.Engine {
	r := gin.Default()
	r.Use(middlewares.Cors())
	api := r.Group("/api")
	api.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"msg": "Welcome to iWeydi!",
		})
	})
	api.GET("/version", controllers.GetVersion)
	api.GET("/ping", controllers.Ping)
	v1 := api.Group("/v1")
	{
		v1.POST("/signup", controllers.Signup)
		v1.POST("/signin", controllers.LoginUser)
		v1.POST("/social/login", controllers.SocialLogin)
		v1.GET("/news/latest", controllers.GetLatestNewsItems)
	}
	authorized := v1.Group("/")
	authorized.Use(middlewares.Auth())
	{
		authorized.GET("/profile", controllers.GetProfile)
		authorized.POST("/question", controllers.AddQuestion)
		authorized.GET("/question/:questionID", controllers.GetQuestion)
	}
	return r
}
