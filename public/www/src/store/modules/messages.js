import {ADD_ALERT_MESSAGE, REMOVE_ALERT_MESSAGE} from '../mutations-constants'

const state = {
  lastID: 0,
  messages: [
  ]
}

const getters = {
  getAlertMessages () {
    return state.messages
  }
}

const mutations = {
  [ADD_ALERT_MESSAGE] (state, payload) {
    if (payload.text) {
      state.lastID++
      let newMsg = payload
      newMsg.id = state.lastID
      newMsg.msgType = (newMsg.msgType ? newMsg.msgType : '')
      state.messages.push(newMsg)
      if (state.messages.length > 10) {
        state.messages.shift()
      }
    }
  },
  [REMOVE_ALERT_MESSAGE] (state, payload) {
    if (typeof payload.id === 'number') {
      for (var i = 0; i < state.messages.length; i++) {
        if (state.messages[i].id === payload.id) {
          state.messages.splice(i, 1)
          break
        }
      }
    }
  }
}

const actions = {
  [ADD_ALERT_MESSAGE] (context, payload) {
    context.commit(ADD_ALERT_MESSAGE, payload)
  },
  [REMOVE_ALERT_MESSAGE] (context, payload) {
    context.commit(REMOVE_ALERT_MESSAGE, payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
