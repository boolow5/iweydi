import {LOADING, IWEYDI_VERSION} from '../mutations-constants'

const state = {
  version: '0.0.5',
  iweydiVersion: '',
  releaseNotes: [
    {date: '28/02/18', note: 'release 0.0.5 - small bug fixes'},
    {date: '27/02/18', note: 'release 0.0.4 - added internationalization with 6 language localizations and more bug fixes'},
    {date: '26/02/18', note: 'release 0.0.3 - added shop page and deployed to web, plus more bug fixes'},
    {date: '24/02/18', note: 'release 0.0.2 - added online dashboard'},
    {date: '21/02/18', note: 'release 0.0.1 - added app version'}
  ],
  loading: false
}

const getters = {
  getiWeydiVersion () {
    return state.iweydiVersion
  },
  getVersion (state) {
    return state.version
  },
  isLoading (state) {
    return state.loading
  }
}

const mutations = {
  [IWEYDI_VERSION] (state, payload) {
    if (payload) {
      state.iweydiVersion = payload.version
    }
  },
  [LOADING] (state, payload) {
    if (payload) {
      state.loading = payload.isLoading === true
    }
  }
}

const actions = {
  [IWEYDI_VERSION] (context, payload) {
    context.commit(IWEYDI_VERSION, payload)
  },
  [LOADING] (context, payload) {
    context.commit(LOADING, payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
