import {USER_JWT_TOKEN, USER_PROFIE_RESPONSE, RESET_USER_AUTH} from '../mutations-constants'

const state = {
  jwtToken: '',
  profile: {
    first_name: '',
    last_name: '',
    gender: '',
    profile_pic: '',
    provider: '',
    profile_completed: '',
    country_of_residence: '',
    nationality: ''
  }
}

const getters = {
  isUserAuthenticated (state) {
    return state.jwtToken && state.jwtToken.length > 100
  },
  getJwtToken (state) {
    return state.jwtToken
  },
  getUserProfile (state) {
    return state.profile
  }
}

const mutations = {
  [RESET_USER_AUTH] (state, payload) {
    state.jwtToken = ''
    state.profile.email = ''
    state.profile.first_name = ''
    state.profile.last_name = ''
    state.profile.gender = ''
    state.profile.profile_pic = ''
    state.profile.profile_completed = ''
    state.profile.country_of_residence = ''
    state.profile.nationality = ''
  },
  [USER_PROFIE_RESPONSE] (state, payload) {
    if (payload) {
      state.jwtToken = payload.last_token
      state.profile.email = payload.email
      state.profile.first_name = payload.first_name
      state.profile.last_name = payload.last_name
      state.profile.gender = payload.gender === true || payload.gender === 'Male' ? 'Male' : 'Female'
      state.profile.profile_pic = payload.profile_pic
      state.profile.profile_completed = payload.profile_completed
      state.profile.country_of_residence = payload.country_of_residence
      state.profile.nationality = payload.nationality
      state.profile.first_name = payload.first_name
      state.profile.first_name = payload.first_name
      localStorage.setItem('user_token', state.jwtToken)
      localStorage.setItem('user_profile', JSON.stringify(state.profile))
    }
  },
  [USER_JWT_TOKEN] (state, payload) {
    if (payload) {
      state.jwtToken = payload.last_token
    }
  }
}

const actions = {
  [RESET_USER_AUTH] (context, payload) {
    context.commit(RESET_USER_AUTH, payload)
  },
  [USER_PROFIE_RESPONSE] (context, payload) {
    context.commit(USER_PROFIE_RESPONSE, payload)
  },
  [USER_JWT_TOKEN] (context, payload) {
    context.commit(USER_JWT_TOKEN, payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
