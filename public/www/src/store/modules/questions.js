const state = {
  latestQuestions: [
    {
      'id': '1',
      'text': 'How are you doing?',
      'url': 'How-are-you-doing',
      'description': 'asking your situation and how you are feeling',
      'authoer': {id: 1, name: 'Mahdi Bolow'},
      'language': 'en',
      'liked_by': ['1', '2'],
      'hated_by': ['3'],
      'love_count': 1100000,
      'hate_count': 1353,
      'comment_count': 3,
      'updated_at': '10:27 18/03/2018'
    },
    {
      'id': '2',
      'text': 'Who was the first president of Somalia? and how did he become the first president',
      'url': 'Who-was-the-first-president-of-Somalia?-and-how-did-he-become-the-first-president',
      'description': 'I need to know little history about the first president of somalia and how he become the president of Somalia',
      'authoer': {id: 1, name: 'Mahdi Bolow'},
      'language': 'en',
      'liked_by': ['1', '2'],
      'hated_by': ['3'],
      'love_count': 1100000,
      'hate_count': 1353,
      'comment_count': 3,
      'updated_at': '10:27 18/03/2018'
    }
  ]
}

const getters = {
  getQuestionByURL (state) {
    return (url) => {
      for (var i = 0; i < state.latestQuestions.length; i++) {
        if (state.latestQuestions[i].url === url) {
          return state.latestQuestions[i]
        }
      }
      return {}
    }
  },
  getLatestQuestions (state) {
    return state.latestQuestions
  }
}

const mutations = {
}

const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
