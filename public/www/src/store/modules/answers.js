const state = {
  latestAnswers: [
    {
      'id': '1',
      'text': 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, facilis necessitatibus. In ad consequuntur deserunt quibusdam animi vitae, quae nulla fugiat, similique eius, laborum temporibus sed. Doloribus nulla perferendis placeat.',
      'url': 'How-are-you-doing',
      'description': 'asking your situation and how you are feeling',
      'authoer': {id: 1, name: 'Mahdi Bolow'},
      'language': 'en',
      'liked_by': ['1', '2'],
      'hated_by': ['3'],
      'love_count': 1100000,
      'hate_count': 1353,
      'comment_count': 3,
      'updated_at': '10:27 18/03/2018',
      'question': 'How are you doing?',
      'question_url': 'How-are-you-doing'
    }
  ]
}

const getters = {
  getLatestAnswers (state) {
    return state.latestAnswers
  },
  getAnswersByID (state) {
    return (id) => {
      state.latestAnswers.map(item => {
        if (item.id === id) {
          return item
        }
      })
    }
  }
}

const mutations = {
}

const actions = {
}

export default {
  state,
  getters,
  mutations,
  actions
}
