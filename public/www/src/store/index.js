import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import auth from './modules/auth'
import messages from './modules/messages'
import questions from './modules/questions'
import answers from './modules/answers'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    auth,
    messages,
    questions,
    answers
  },
  strict: true
})
