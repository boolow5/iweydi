import Vue from 'vue'
import Router from 'vue-router'

// const Foo = resolve => require(['../views/hello.vue'], resolve);
// import hello from '../views/hello.vue';

import Home from '../components/Home.vue'
import Login from '../components/Login.vue'
import Terms from '../components/Terms.vue'
import Privacy from '../components/Privacy.vue'
import Question from '../components/Question.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    { name: 'home', path: '/', component: Home },
    { name: 'Login', path: '/login', component: Login },
    { name: 'Terms', path: '/terms', component: Terms },
    { name: 'Privacy', path: '/privacy', component: Privacy },
    { name: 'Question', path: '/question', component: Question }
  ]
})
