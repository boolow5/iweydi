import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import Cordova from './Cordova.js'

import store from './store'
import router from './router'
import { sync } from 'vuex-router-sync'
import vuexI18n from 'vuex-i18n'
import GoogleAuth from 'vue-google-oauth'

import Somali from '../translations/locales/1_so'
import Arabic from '../translations/locales/2_ar'

sync(store, router)
Vue.use(vuexI18n.plugin, store)
Vue.i18n.add('so', Somali)
Vue.i18n.add('ar', Arabic)

Vue.use(GoogleAuth, { client_id: '485791796617-35sul1ltnsj294s97b7k9sfdrho9jnqu.apps.googleusercontent.com' })
Vue.googleAuth().load()
Vue.googleAuth().directAccess()

export const EventBus = new Vue()

// Load Vue instance
export default new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
  mounted () {
    Cordova.initialize()
  }
})
