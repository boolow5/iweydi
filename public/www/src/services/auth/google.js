const GOOGLE_ERROR_CODES = [
  {
    number: 4,
    name: 'SIGN_IN_REQUIRED',
    message: 'google returned login is required'
  },
  {
    number: 5,
    name: 'INVALID_ACCOUNT',
    message: 'google returned invalid account'
  },
  {
    number: 6,
    name: 'RESOLUTION_REQUIRED',
    message: 'google returned resolution is required'
  },
  {
    number: 7,
    name: 'NETWORK_ERROR',
    message: 'connecting to google failed'
  },
  {
    number: 8,
    name: 'INTERNAL_ERROR',
    message: 'google returned an internal error'
  },
  {
    number: 10,
    name: 'DEVELOPER_ERROR',
    message: 'google returned developer configuration error'
  },
  {
    number: 13,
    name: 'ERROR',
    message: 'google returned error'
  },
  {
    number: 14,
    name: 'INTERRUPTED',
    message: 'google returned interrupted error'
  },
  {
    number: 15,
    name: 'TIMEOUT',
    message: 'google returned timeout error'
  },
  {
    number: 16,
    name: 'CANCELED',
    message: 'google returned request canceled error'
  },
  {
    number: 17,
    name: 'API_NOT_CONNECTED',
    message: 'google returned connection failed error'
  }
]

var getGoogleErrorByNumber = function (errNumber) {
  for (var i = 0; i < GOOGLE_ERROR_CODES.length; i++) {
    if (GOOGLE_ERROR_CODES[i].number === parseInt(errNumber)) {
      return GOOGLE_ERROR_CODES[i]
    }
  }
  return { number: errNumber, name: 'ERROR', message: 'google returned error' }
}

var getGoogleErrorToString = function (errNumber) {
  for (var i = 0; i < GOOGLE_ERROR_CODES.length; i++) {
    if (GOOGLE_ERROR_CODES[i].number === parseInt(errNumber)) {
      return `${GOOGLE_ERROR_CODES[i].number} ${GOOGLE_ERROR_CODES[i].name} ${GOOGLE_ERROR_CODES[i].message}!`
    }
  }
  return `${errNumber} ERROR google returned error`
}

const googleConfig = {
  webClientId: '485791796617-35sul1ltnsj294s97b7k9sfdrho9jnqu.apps.googleusercontent.com'
}

/**
 * @name googleNativeLogin.
 * @description handles android/iOS native google authentication
 * @param {function} successCallback Success callback, which will get data Object as parameter.
 * @param {function} errorCallback Error callback, which will get error as parameter.
 */
var googleNativeLogin = function (successCallback, errorCallback) {
  if (!!window.plugins && window.plugins.googleplus) {
    window.plugins.googleplus.getSigningCertificateFingerprint(function (fingerprint) {
      console.log(`fingerprint: ${fingerprint}`)
    })
    window.plugins.googleplus.login({
      'webClientId': googleConfig.webClientId,
      'offline': true
    }, successCallback, err => {
      errorCallback(getGoogleErrorToString(err))
    })
  } else {
    console.log('googleNativeLogin is not available', window.plugins)
    errorCallback(new Error('Googleplus plugin is not available'))
  }
}

/**
 * @name googleNativeLogout.
 * @description handles android/iOS native google logout
 * @param {function} successCallback Success callback, which will get data Object as parameter.
 * @param {function} errorCallback Error callback, which will get error as parameter.
 */
var googleNativeLogout = function (successCallback, errorCallback) {
  if (!!window.plugins && window.plugins.googleplus) {
    window.plugins.googleplus.logout(data => {
      if (data && data.accessToken) {
        successCallback({access_token: data.accessToken, data: data})
      } else {
        successCallback(data)
      }
    }, err => {
      errorCallback(getGoogleErrorToString(err))
    })
  } else {
    console.log('googleNativeLogin is not available', window.plugins)
    errorCallback(new Error('Googleplus plugin is not available'))
  }
}

/**
 * @name googleBrowserLogin.
 * @description handles browser login using google authentication
 * @param {function} $googleAuth The googleAuth instance function from Vue properties.
 * @param {function} successCallback Success callback, which will get access_token as parameter.
 * @param {function} errorCallback Error callback, which will get error as parameter.
 */
var googleBrowserLogin = function ($googleAuth, successCallback, errorCallback) {
  console.log('googleBrowserLogin', $googleAuth)
  $googleAuth().signIn(authorizationCode => {
    console.log('googleBrowserLogin onSignInSuccess', authorizationCode)
    if (!authorizationCode.Zi) {
      // invalid response from google
      errorCallback(new Error('Sorry, Google returned an invalid response'))
      return
    }

    if (!authorizationCode.Zi.access_token) {
      // no token
      errorCallback(new Error('Sorry, Google returned an invalid token'))
      return
    }
    successCallback({access_token: authorizationCode.Zi.access_token})
  }, errorCallback)
}

/**
 * @name googleBrowserLogout.
 * @description handles browser google logout
 * @param {function} $googleAuth The googleAuth instance function from Vue properties.
 * @param {function} successCallback Success callback.
 * @param {function} errorCallback Error callback.
 */
var googleBrowserLogout = function ($googleAuth, successCallback, errorCallback) {
  $googleAuth().signOut(successCallback, errorCallback)
}

export default {
  googleConfig,
  googleNativeLogin,
  googleNativeLogout,
  googleBrowserLogin,
  googleBrowserLogout,
  getGoogleErrorByNumber,
  getGoogleErrorToString
}
