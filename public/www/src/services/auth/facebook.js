const facebookConfig = {
  appId: '1816014891772453',
  cookie: true,
  xfbml: true,
  version: 'v5.0'
}

var facebookInit = function (successCallback, errorCallback) {
  if (window.FB) {
    window.FB.init(facebookConfig)
    window.FB.getLoginStatus(function (response) {
      successCallback({response})
    })
  } else {
    errorCallback(new Error('Facebook is not initialized'))
  }
}

var facebookLogin = function (successCallback, errorCallback) {
  if (window.facebookConnectPlugin) {
    console.log('facebookConnectPlugin', window.facebookConnectPlugin)
    window.facebookConnectPlugin.getLoginStatus(data => {
      console.log('Facebook getLoginStatus response', data)
      if (data.status === 'connected') {
        if (data.authResponse && data.authResponse.accessToken) {
          successCallback({token: data.authResponse.accessToken})
        }
      } else if (data.response && data.response.status === 'connected') {
        if (data.response.authResponse && data.response.authResponse.accessToken) {
          successCallback({token: data.response.authResponse.accessToken})
        }
      } else {
        window.facebookConnectPlugin.login(
          ['email,public_profile,user_birthday,user_friends,user_location'],
          successCallback, errorCallback)
      }
    }, err => {
      console.log('Facebook getLoginStatus error', err)
    })
  } else if (window.FB) {
    console.log('window.FB', window.FB)
    facebookInit(data => {
      console.log('facebookInit', data)
      if (data.response.status === 'connected') {
        console.log('facebook already connected')
        if (data.response && data.response.authResponse && data.response.authResponse.accessToken) {
          successCallback({token: data.response.authResponse.accessToken})
        }
      } else {
        console.log('facebook loggin in...')
        window.FB.login(function (data) {
          console.log('login response', data)
          if (data.authResponse && data.authResponse.accessToken) {
            successCallback({token: data.authResponse.accessToken})
          } else if (data.response && data.response.authResponse && data.response.authResponse.accessToken) {
            successCallback({token: data.response.authResponse.accessToken})
          } else {
            errorCallback(data)
          }
        }, {scope: 'email,public_profile,user_birthday,user_friends,user_location'})
      }
    }, err => {
      console.log('facebookInit error', err)
      errorCallback(err)
    })
  } else {
    errorCallback(new Error('Facebook is not initialized'))
  }
}

var getFacebookProfile = function (successCallback, errorCallback) {
  if (window.FB) {
    window.FB.api('/me', function (response) {
      console.log('getProfile response', response)
      successCallback({response})
    })
  } else {
    errorCallback(new Error('Facebook is not initialized'))
  }
}

export default {
  facebookConfig,
  facebookInit,
  facebookLogin,
  getFacebookProfile
}
