import Q from 'q'
import axios from 'axios'
import common from './common'

var socialLogin = function (provider, token) {
  console.log('socialLogin constructUrl', common)
  var deferred = Q.defer()
  axios({
    method: 'POST',
    url: common.constructUrl(common.IWEYDI, '/social/login'),
    data: {provider: provider, access_token: token}
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  socialLogin
}
