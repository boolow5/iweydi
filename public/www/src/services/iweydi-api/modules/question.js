import Q from 'q'
import axios from 'axios'
import common from './common'

var AddQuestion = function (data, token) {
  console.log('AddQuestion')
  var deferred = Q.defer()
  axios({
    method: 'POST',
    url: common.constructUrl(common.IWEYDI, '/question'),
    headers: {Authorization: `Bearer ${token}`},
    data
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var UpdateQuestion = function (data, token) {
  console.log('AddQuestion')
  var deferred = Q.defer()
  axios({
    method: 'PUT',
    url: common.constructUrl(common.IWEYDI, '/question'),
    headers: {Authorization: `Bearer ${token}`},
    data
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var GetQuestion = function (id, token) {
  console.log('AddQuestion')
  var deferred = Q.defer()
  axios({
    method: 'GET',
    url: common.constructUrl(common.IWEYDI, `/question/${id}`),
    headers: {Authorization: `Bearer ${token}`}
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var DeleteQuestion = function (id, token) {
  console.log('AddQuestion')
  var deferred = Q.defer()
  axios({
    method: 'DELETE',
    url: common.constructUrl(common.IWEYDI, `/question/${id}`),
    headers: {Authorization: `Bearer ${token}`}
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  AddQuestion,
  GetQuestion,
  UpdateQuestion,
  DeleteQuestion
}
