const IWEYDI = 'iweydi'

const IS_LOCAL = true
const iweydiServiceRemoteURL = 'http://api.iweydi.com'
const iweydiServiceLocalURL = 'http://localhost:8080'
const NS = '/api/v1'
const BARE = 'bare'

var constructUrl = function (targetService, route) {
  if (String(route).indexOf('/') !== 0) {
    route = '/' + route
  }
  let url = ''
  console.log(`constructUrl ${IS_LOCAL ? 'local' : 'remote'} target: ${targetService}, route: ${route}`)
  switch (targetService) {
    case IWEYDI:
      if (IS_LOCAL === true) {
        url += iweydiServiceLocalURL
      } else {
        url += iweydiServiceRemoteURL
      }
      url += NS
      // check if it starts with /
      return url + route
    case BARE:
      if (IS_LOCAL === true) {
        url += iweydiServiceLocalURL
      } else {
        url += iweydiServiceRemoteURL
      }
      return url + route
  }
  return `${iweydiServiceLocalURL}${NS}${route}`
}

export default {
  IWEYDI,
  IS_LOCAL,
  iweydiServiceRemoteURL,
  iweydiServiceLocalURL,
  constructUrl,
  BARE
}
