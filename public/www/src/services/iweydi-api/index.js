import auth from './modules/auth'
import Question from './modules/question'

export default {
  auth,
  Question
}
