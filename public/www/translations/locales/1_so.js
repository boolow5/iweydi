const localizations = {
  'iWeydi is platform for exchanging knowledge and ideas': 'iWeydi waa gole la isku weydaarsado fikirka iyo aqoonta',
  'Any answer you share on the platform can be seen by anyone': 'Jawaabkasta aad soo dhigto golahan waxaa arki kara cid kasta',
  'You can hide your identity when you think your answer might bring security risk to you or your family': 'Waad qarin kartaa shaqsiyadaada hadii aad u baqeyso noloshaada ama tan reerkaaga',
  'We will not share your personal info with other third-parties when you request to hide it': 'Ma muujin doonno shaqsiyadaada marka aad codsato in aan qarino kadib',
  'Questions and answers you set to anonymous mode will not be associated with your profile': 'Su\'aalaha iyo jawaabaha aad soo geliso si qarsoodi ah lama xiriirin doono shaqsiyadaada',
  'Avoid sharing information that can be used to identify you when you are answering or asking in anonymous mode': 'Iska ilaali inaad kudarto macluumaad lagugu aqoonsan karo marka aad qoreyso su\'aal ama jawaab si qarsoodi ah',
  'When third party requests to publish your answer to a question, you have the right to refuse it': 'Hadi cid-kale ay kaa codsato in jawaab aad qortay lagu soo saaro website ama jaraa\'id waxaad xaq uleedahay inaad ka diido',
  'All information you share on iWeydi belongs to you and you are solely responsible': 'Macluumaad kasta aad ku wadaagto iWeydi adiga ayaa milki uleh masuuliyadeedana kaligaa ayaa qaadaya',
  'iWeydi is a platform for exchanging knowledge and ideas.': 'iWeydi waa gole la isku weydaarsado fikirka iyo aqoonta',
  'You are responsible for anything you share on iWeydi': 'Adiga ayaa ka masuul ah waxkasta aad ku wadaagto iWeydi',
  'You agree not to help or promote violance': 'Waxaad aqbashay in aadan caawin ama dhiiri-gelin qalalaase ama nabad-darro',
  'If the filter identifies your usage as unethical or illegal iWeydi can cease your service and ban you from using the platform': 'Hadii filter-ka uu adeegsigaaga u aqoonsado mid anshaxa ama sharciga kasoo horjeedo iWeydi waxay xaq u leeyahay in adeega lagaa joojiyo oo lagaana mamnuuco golahan',
  'You share accurate information and your true opinion when answering questions posted on iWeydi': 'In macluumaadka aad ku wadaageyso iWeydi uu yahay mid xaqiiqo ah ama fikir adiga aad qabto oo kuu gaar ah',
  'If the information you write is pure opinion tell that in your answers': 'Hadi aad waxa aad wadaageyso uu fikirkaaga yahay ku cadey jawaabta in waxani fikirkaaga yihiin',
  'Privacy policy': 'Siyaasada Sir-dhowrka',
  'Hide': 'Qari',
  'Login': 'Gal',
  'Sign up': 'Is-diiwaangeli',
  'Facebook page': 'Bogga Facebook',
  'Terms of Service': 'Shuruudaha adeegsiga',
  'Language': 'Luqadda',
  'Home': 'Hoyga'
}

export default localizations
