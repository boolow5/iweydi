const localizations = {
  'iWeydi is platform for exchanging knowledge and ideas': 'iWeydi هي منصة لتبادل المعرفة والأفكار',
  'Any answer you share on the platform can be seen by anyone': 'يمكن لأي شخص أن يشاهد أي إجابة تشاركها على المنصة',
  'You can hide your identity when you think your answer might bring security risk to you or your family': 'يمكنك إخفاء هويتك عندما تعتقد أن إجابتك قد تجلب لك مخاطر أمنية أو عليك',
  'We will not share your personal info with other third-parties when you request to hide it': 'لن نشارك معلوماتك الشخصية مع أطراف ثالثة أخرى عندما تطلب إخفاءها "',
  'Questions and answers you set to anonymous mode will not be associated with your profile': 'لن تقترن الأسئلة والأجوبة التي تعينها على الوضع المجهول بملفك الشخصي',
  'Avoid sharing information that can be used to identify you when you are answering or asking in anonymous mode': 'تجنب مشاركة المعلومات التي يمكن استخدامها لتحديد هويتك عند الرد أو السؤال في وضع مجهول',
  'When third party requests to publish your answer to a question, you have the right to refuse it': 'عندما يطلب طرف ثالث نشر إجابتك على سؤال ما ، لديك الحق في رفضه',
  'All information you share on iWeydi belongs to you and you are solely responsible': 'جميع المعلومات التي تشاركها في iWeydi هي ملك لك وأنت المسؤول الوحيد',
  'iWeydi is a platform for exchanging knowledge and ideas.': 'iWeydi هو منصة لتبادل المعرفة والأفكار.',
  'You are responsible for anything you share on iWeydi': 'أنت مسؤول عن أي شيء تشاركه في iWeydi',
  'You agree not to help or promote violance': 'أنت توافق على عدم المساعدة أو الترويج للانتهاك',
  'If the filter identifies your usage as unethical or illegal iWeydi can cease your service and ban you from using the platform': 'إذا حدد المرشح استخدامك على أنه غير أخلاقي أو غير قانوني ، فيمكن لـ iWeydi إيقاف الخدمة ومنعك من استخدام النظام الأساسي',
  'You share accurate information and your true opinion when answering questions posted on iWeydi': 'أنت تشارك معلومات دقيقة ورأيك الحقيقي عند الإجابة على الأسئلة المنشورة على iWeydi',
  'If the information you write is pure opinion tell that in your answers': 'إذا كانت المعلومات التي تكتبها هي رأي خالص ، فأخبر ذلك في إجاباتك',
  'Privacy policy': 'سياسة الخصوصية',
  'Hide': 'إخفاء',
  'Login': 'تسجيل الدخول',
  'Sign up': 'سجل',
  'Facebook page': 'صفحة الفيسبوك',
  'Terms of Service': 'شروط الخدمة',
  'Language': 'لغة',
  'Home': 'الصفحة الرئيسية'
}

export default localizations
