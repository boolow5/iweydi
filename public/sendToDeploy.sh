#!/bin/sh

# variables
HOST_NAME=iweydi.com
USER_NAME=deploy
BASE_DIR=$PWD
DESTINATION_DIR=/var/www/dashboard/pages/
SOURCE_DIR=$BASE_DIR/platforms/browser/www/
echo "Sending files to server: ${HOST_NAME} with user: ${USER_NAME}"
echo "project directory: ${BASE_DIR}"
echo "source directory: ${SOURCE_DIR}"
echo "server's destination directory: ${DESTINATION_DIR}"

rsync -auv -e ssh --progress $SOURCE_DIR/* $USER_NAME@$HOST_NAME:$DESTINATION_DIR
