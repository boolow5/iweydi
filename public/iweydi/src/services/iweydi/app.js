import axios from 'axios'
import Q from 'q'
import urls from './urls'

var GetVersion = function () {
  var deferred = Q.defer()
  axios({
    method: 'GET',
    url: urls.DOMAIN + '/api/version'
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}
export default {
  GetVersion
}
