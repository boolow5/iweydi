import Q from 'q'
import axios from 'axios'
import urls from './urls'

var GetLatestNews = function (category, page, limit) {
  var deferred = Q.defer()
  let url = urls.BASE_URL + '/news/latest'
  if (category || page || limit) {
    url += encodeQueries([
      {key: 'category', value: category},
      {key: 'page', value: page},
      {key: 'limit', value: limit}
    ])
  }
  console.log('URL', url)
  axios({
    method: 'GET',
    url: url
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

let encodeQueries = (queries) => {
  let result = ''
  if (queries.length) {
    result += '?'
  }
  for (var i = 0; i < queries.length; i++) {
    if (!queries[i].key) {
      continue
    }
    if (!queries[i].value) {
      continue
    }
    if (i === 0) {
      result += `${queries[i].key}=${queries[i].value}`
    } else {
      result += `&${queries[i].key}=${queries[i].value}`
    }
  }
  return result
}

export default {
  GetLatestNews
}
