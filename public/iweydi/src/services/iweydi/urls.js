const IS_LOCAL = false
const LOCAL_DOMAIN = 'http://localhost:8080'
const REMOTE_DOMAIN = 'http://iweydi.com'
const LOCAL_URL = LOCAL_DOMAIN + '/api/v1'
const REMOTE_URL = REMOTE_DOMAIN + '/api/v1'

var BASE_URL = REMOTE_URL
var DOMAIN = REMOTE_DOMAIN

if (IS_LOCAL) {
  BASE_URL = LOCAL_URL
  DOMAIN = LOCAL_DOMAIN
}

export default {
  DOMAIN,
  BASE_URL,
  LOCAL_URL,
  REMOTE_URL
}
