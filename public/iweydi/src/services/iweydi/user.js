import Q from 'q'
import axios from 'axios'
import urls from './urls'

var Login = function (email, pass) {
  var deferred = Q.defer()
  axios({
    method: 'POST',
    data: {'email': email, 'password': pass},
    url: urls.BASE_URL + '/signin'
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var Signup = function (userData) {
  var deferred = Q.defer()
  axios({
    method: 'POST',
    data: userData,
    url: urls.BASE_URL + '/signup'
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

var SocialLogin = function (data) {
  var deferred = Q.defer()
  axios({
    method: 'POST',
    data: data,
    url: urls.BASE_URL + '/social/login'
  }).then(resp => {
    deferred.resolve(resp)
  }).catch(err => {
    deferred.reject(err)
  })
  return deferred.promise
}

export default {
  Login,
  Signup,
  SocialLogin
}
