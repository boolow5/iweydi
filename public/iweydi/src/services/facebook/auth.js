import Q from 'q'

var SCOPES = 'email,public_profile'

var Init = function () {
  window.FB.init({
    appId: '1816014891772453',
    cookie: true,
    xfbml: true,
    version: 'v2.9'
  })
}

var GetLoginStatus = function () {
  var deferred = Q.defer()
  window.FB.getLoginStatus(function (response) {
    console.log('FACEBOOK LOGIN getLoginStatus')
    console.log(response)
    deferred.resolve(response)
  })
  return deferred.promise
}

var StatusChanged = function (response) {
  var deferred = Q.defer()
  console.log('statusChangeCallback')
  console.log(response)
  if (response.status === 'connected') {
    console.log('authResponse')
    deferred.resolve(response)
  } else if (response.status === 'not_authorized') {
    deferred.reject(response)
  } else {
    deferred.reject(response)
  }
  return deferred.promise
}

var GetProfile = function () {
  var deferred = Q.defer()
  window.FB.api('/me', function (response) {
    console.log('getProfile response')
    console.log(response)
    // vm.$set(vm, 'profile', response)
    deferred.resolve(response)
  })
  return deferred.promise
}

var Login = function () {
  var deferred = Q.defer()
  window.FB.login(function (response) {
    console.log('login response')
    console.log(response)
    deferred.resolve(response)
  }, {scope: SCOPES})
  return deferred.promise
}

var Logout = function () {
  var deferred = Q.defer()
  window.FB.logout(function (response) {
    console.log('logout response')
    console.log(response)
    deferred.resolve(response)
  })
  return deferred.promise
}

export default {
  Init,
  GetLoginStatus,
  StatusChanged,
  GetProfile,
  Login,
  Logout
}
