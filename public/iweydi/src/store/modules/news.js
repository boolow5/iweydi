const state = {
  newsItems: []
}

const getters = {
  getNewsItems: (state) => {
    return state.newsItems
  }
}

const mutations = {
  setNewsItem (state, context) {
    // console.log('setNewsItem', context)
    state.newsItems.push(context)
  },
  setNewsItems (state, context) {
    console.log('setNewsItems', context)
    state.newsItems = context
  }
}

export default {
  state,
  getters,
  mutations
}
