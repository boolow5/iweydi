const state = {
  token: '',
  userData: {},
  social: {
    authorized: false,
    profile: {},
    provider: ''
  }
}

const getters = {
  getAuthentication (state) {
    return state.token !== ''
  },
  getUserData (state) {
    return state.userData
  }
}

const mutations = {
  setSocialProfile (state, context) {
    if (JSON.stringify(context) === '{}') {
      return
    }
    if (context.provider && context.profile) {
      state.social.provider = context.provider
      state.social.profile = context.profile
    }
  },
  setSocialAuthorized (state, context) {
    state.social.authorized = Boolean(context)
  },
  clearUserData (state, context) {
    state.userData = {}
    state.token = ''
    localStorage.clear()
  },
  setToken (state, context) {
    localStorage.setItem('user_token', context)
    state.token = context
  },
  setUserData (state, context) {
    localStorage.setItem('user_data', JSON.stringify(context))
    state.userData = context
  }
}

export default {
  state,
  getters,
  mutations
}
