const state = {
  version: '0.0.0',
  showSidebar: false
}

const getters = {
  showSidebar (state) {
    return state.showSidebar
  },
  getVersion (state) {
    return state.version
  }
}

const mutations = {
  toggleSidebar (state, context) {
    state.showSidebar = !state.showSidebar
  },
  setVersion (state, context) {
    context = context.replace('\'', '')
    let parts = context.split('\\:')
    if (parts.length) {
      state.version = parts[0]
    }
  }
}

export default {
  state,
  getters,
  mutations
}
