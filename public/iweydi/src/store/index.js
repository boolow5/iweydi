import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import user from './modules/user'
import countries from './modules/countries'
import news from './modules/news'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    user,
    countries,
    news
  },
  strict: true
})
