package models

import (
	"gopkg.in/mgo.v2/bson"
)

// QuestionFeed temporarily hold all questions a user need to see
type QuestionFeed struct {
	UserID      bson.ObjectId   `bson:"_id,omitempty" json:"id" `
	QuestionIDs []bson.ObjectId `json:"question_ids" bson:"question_ids"`
}

// AnswerFeed temporarily hold all questions a user need to see
type AnswerFeed struct {
	UserID    bson.ObjectId   `bson:"_id,omitempty" json:"id" `
	AnswerIDs []bson.ObjectId `json:"answer_ids" bson:"answer_ids"`
}

// Notification is a push message sent to users to inform new info or something that intrests them
type Notification struct {
	ID bson.ObjectId `bson:"_id,omitempty" json:"id" `

	ActionType string  `json:"Activity" bson:"Activity"`
	Doer       Profile `json:"action_doer" bson:"action_doer"`
	Receiver   *User   `json:"receiver" bson:"receiver"`
	Seen       bool    `json:"seen" bson:"seen"`

	Timestamp `bson:",inline"`
}

// Tag represents tags for creating relation between questions
type Tag struct {
	ID   bson.ObjectId `bson:"_id,omitempty" json:"id" `
	Name string        `bson:"name" json:"name"`
}
