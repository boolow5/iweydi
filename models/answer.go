package models

import (
	"bitbucket.org/boolow5/iweydi/db"
	"gopkg.in/mgo.v2/bson"
)

// Answer represents an answer object
type Answer struct {
	ID bson.ObjectId `bson:"_id,omitempty" json:"id" `

	Text   string  `json:"text" orm:"size(1000)"`
	Author Profile `json:"author" orm:"rel(fk);on_delete(cascade)"`

	QuestionID bson.ObjectId `json:"question_id" bson:"question_id"`
	Comments   []Comment     `json:"comments" bson:"comments"`

	LoveCount    int `json:"love_count" bson:"love_count"`
	HateCount    int `json:"hate_count" bson:"hate_count"`
	CommentCount int `json:"comment_count" bson:"comment_count"`

	Timestamp `bson:",inline"`
}

// GetAnswersByQuestionID finds all answers by question ID
func GetAnswersByQuestionID(qID bson.ObjectId) ([]Answer, error) {
	answers := []Answer{}
	err := db.FindAllSort(ClAnswer, &bson.M{"question_id": qID}, answers, "-updated_at")
	return answers, err
}
