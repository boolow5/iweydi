package models

import (
	"gopkg.in/mgo.v2/bson"
)

// Comment represents a comment about post, question or answer
type Comment struct {
	ID bson.ObjectId `bson:"_id,omitempty" json:"id" `

	Text       string        `json:"text" bson:"text"`
	Author     *User         `json:"author" bson:"author"`
	ParentType string        `json:"parent_type" bson:"parent_type"`
	ParentID   bson.ObjectId `json:"parent_id" bson:"parent_id"`
	Replies    []Comment     `json:"replies" bson:"replies"`

	LoveCount    int `json:"love_count" bson:"love_count"`
	HateCount    int `json:"hate_count" bson:"hate_count"`
	CommentCount int `json:"comment_count" bson:"comment_count"`

	Timestamp `bson:",inline"`
}

// String returns the text in the comment
func (c *Comment) String() string {
	return c.Text
}

// Conference represents a group of questions asked to the same guest or guests to answer in one session
type Conference struct {
	ID bson.ObjectId `bson:"_id,omitempty" json:"id" `

	Opening int64 `json:"opening" bson:"opening"`
	Closing int64 `json:"closing" bson:"closing"`

	Question []Question `json:"questions" bson:"questions"`

	Guests []User `json:"guest" bson:"guest"`

	Timestamp `bson:",inline"`
}
