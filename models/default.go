package models

import (
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/boolow5/iweydi/db"
)

const (
	// ClUser collection name
	ClUser = "user"
	// ClWebsite collection name
	ClWebsite = "website"
	// ClNewsItem collection name
	ClNewsItem = "news_item"
	// ClQuestion collection name
	ClQuestion = "question"
	// ClAnswer collection name
	ClAnswer = "answer"
)

// Version number
var Version string

// Timestamp represents embedded object for update, delete and create timestamps
type Timestamp struct {
	CreatedAt int64 `json:"created_at" bson:"created_at"`
	UpdatedAt int64 `json:"updated_at" bson:"updated_at"`
	DeletedAt int64 `json:"-" bson:"deleted_at"`
}

// CountItems counts items in the colName using query as filter
func CountItems(colName string, query bson.M) int {
	if colName == "" {
		return -1
	}
	i, err := db.Count(colName, &query)
	if err != nil {
		return -1
	}
	return i
}
