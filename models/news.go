package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"

	"bitbucket.org/boolow5/iweydi/config"
	"bitbucket.org/boolow5/iweydi/db"
	"gopkg.in/mgo.v2/bson"
)

// Website is an object representing a website
type Website struct {
	ID            bson.ObjectId `json:"id" bson:"_id,omitempty"`
	URL           string        `json:"url" bson:"url"`
	Name          string        `json:"name" bson:"name"`
	Category      string        `json:"category" bson:"category"`
	NewsItems     []News        `json:"news_items" bson:"news_items"`
	BaseSelector  string        `json:"base_selector"`
	TitleSelector string        `json:"title_selector"`
	Timestamp     `bson:",inline"`
}

// News is an object representing an article
type News struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	URL       string        `json:"url" bson:"url"`
	Title     string        `json:"title" bson:"title"`
	Website   Website       `json:"website" bson:"website"`
	Views     int           `json:"views" bson:"views"`
	Timestamp `bson:",inline"`
}

// Init initializes all required data and configurations
func Init() {
	fmt.Println("news.Init()")
	fmt.Println(db.AddUniqueIndex(ClNewsItem, "url"))
	fmt.Println(db.AddUniqueIndex(ClWebsite, "url"))

	allWebsites := []Website{}
	err := db.FindAll(ClWebsite, &bson.M{}, &allWebsites)
	if err != nil {
		panic("Failed to find websites")
	}

	if len(allWebsites) < 1 {
		allWebsites = laodSitesFromFile()
	}

	delay := time.Duration(config.Get().ScrapperDelay) * time.Minute
	for {
		for i := 0; i < len(allWebsites); i++ {
			go allWebsites[i].Visit()
			// time.Sleep(1 * time.Second)
			// fmt.Println("!")
		}
		fmt.Println("___________________________________")
		fmt.Printf("Done visiting %d websites. Will visit after %s\n", len(allWebsites), delay)
		time.Sleep(delay)
	}
}

func laodSitesFromFile() []Website {
	sites := []Website{}
	filePath := ""
	if len(os.Args) > 1 {
		filePath = filepath.Join(os.Args[1], "websites.json")
	} else {
		exe, err := os.Executable()
		if err != nil {
			panic(err)
		}
		exePath := filepath.Dir(exe)
		fmt.Println("\nexePath\n:", exePath)
		filePath = filepath.Join(exePath, "websites.json")
	}

	fileData, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(fileData, &sites)
	if err != nil {
		panic(err)
	}

	for i := 0; i < len(sites); i++ {
		fmt.Println("site:", sites[i].Name, "category:", sites[i].Category)
	}

	return sites
}

// Visit fetches data from website
func (w *Website) Visit() error {
	fmt.Println("w.Visit()", w.URL)

	// save website if not saved before
	w.Save()
	doc, err := goquery.NewDocument(w.URL)
	if err != nil {
		fmt.Println("Error fetching website. Reason:", err)
		return err
	}
	var currentItem *News
	doc.Find(w.BaseSelector).Each(func(i int, section *goquery.Selection) {
		part := section.Find(w.TitleSelector)
		text := strings.TrimSpace(part.Text())
		link, ok := part.Attr("href")
		if ok {
			// fmt.Printf("\n%d\t%s\n\t%s\n", i, text, link)
			// fmt.Printf("site: %s: %s\t", w.Name, w.URL)
			fmt.Printf("%s", ".")
			currentItem = &News{Title: text, URL: link, Website: *w}

			time.Sleep(100 * time.Millisecond)
			go currentItem.Save()
		}

	})
	fmt.Println("Done visiting", w.Name)
	return nil
}

// GetNewsItem fetches news articles from database
func GetNewsItems(page, limit int, category string) ([]News, error) {
	var items []News
	var err error
	if category == "all" {
		err = db.FindPageWithLimit(ClNewsItem, &bson.M{}, page, limit, &items, "-created_at")
	} else {
		err = db.FindPageWithLimit(ClNewsItem, &bson.M{"website.category": bson.M{"$regex": bson.RegEx{Pattern: category, Options: "i"}}}, page, limit, &items, "-created_at")
	}

	return items, err
}

// Save saves website in the database
func (w *Website) Save() error {
	// fmt.Println("news.Save()")
	w.ID = bson.NewObjectId()
	now := time.Now()
	w.CreatedAt = now.Unix()
	w.UpdatedAt = now.Unix()

	err := db.Create(ClWebsite, w)
	if err != nil {
		if db.IsDupError(err) {
			fmt.Printf("%s", "^")
			return err
		}
		fmt.Printf("%s", "v")
		return err
	}
	fmt.Printf("%s", "-")
	return nil
}

// Save stores news item in the database
func (n *News) Save() error {
	// fmt.Println("news.Save()")
	n.ID = bson.NewObjectId()
	now := time.Now()
	n.CreatedAt = now.Unix()
	n.UpdatedAt = now.Unix()

	err := db.Create(ClNewsItem, n)
	if err != nil {
		if db.IsDupError(err) {
			fmt.Printf("%s", "#")
			return err
		}
		fmt.Printf("%s", "*")
		return err
	}
	fmt.Printf("%s", "+")
	return nil
}
func shorten(str string, max int) string {
	if len(str) <= max {
		return str
	}
	return str[:max] + "..."
}
