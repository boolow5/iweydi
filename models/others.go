package models

import (
	"gopkg.in/mgo.v2/bson"
)

// Language have the name and code of supported languages
type Language struct {
	ID   bson.ObjectId `bson:"_id,omitempty" json:"id" `
	Name string        `json:"name"`
	Code string        `json:"code"`

	Timestamp `bson:",inline"`
}

// UserRelations keeps track of relations between users
type UserRelations struct {
	UserID    bson.ObjectId `bson:"user_id,omitempty" json:"user_id" `
	Followers []Profile     `bson:"followers" json:"followers"`
	Following []Profile     `bson:"following" json:"following"`
}

// Topic is used to group questions
type Topic struct {
	ID bson.ObjectId `bson:"_id,omitempty" json:"id" `

	Name          string `json:"name" bson:"name"`
	FollowerCount int    `json:"followers" bson:"followers"`
	Parent        *Topic `json:"parent_topic" bson:"parent_topic"`

	Followers []bson.ObjectId `json:"followers_ids" bson:"followers_ids"`

	Timestamp `bson:",inline"`
}

func (t *Topic) String() string {
	return t.Name
}
