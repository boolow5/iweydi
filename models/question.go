package models

import (
	"errors"
	"strings"

	"bitbucket.org/boolow5/iweydi/db"

	"gopkg.in/mgo.v2/bson"
)

// Question object
type Question struct {
	ID bson.ObjectId `bson:"_id,omitempty" json:"id" `

	Text        string  `json:"text" bson:"text"`
	URL         string  `json:"url" bson:"url"`
	Description string  `json:"description" bson:"description"`
	Author      Profile `json:"author" bson:"authoer"`

	Language   Language   `json:"language" bson:"language"`
	Conference Conference `json:"Conference" bson:"conference"`
	Tags       []Tag      `json:"tags" bson:"tags"`

	Comments []bson.ObjectId `json:"comments" bson:"comments"`

	Likers []bson.ObjectId `json:"i_liked_this" bson:"liked_by"`
	Haters []bson.ObjectId `json:"i_hated_this" bson:"hated_by"`

	LoveCount    int `json:"love_count" bson:"love_count"`
	HateCount    int `json:"hate_count" bson:"hate_count"`
	CommentCount int `json:"comment_count" bson:"comment_count"`

	Timestamp `bson:",inline"`
}

// SetText sets the text and creates unique url for text
func (q *Question) SetText(text string) {
	q.Text = text
	q.URL = strings.Replace(q.Text, " ", "-", -1)
	q.URL = strings.Replace(q.URL, "?", "", -1)
	if len(q.ID.Hex()) > 10 {
		// last 10 characters of the ID to prevent similar question url
		IDParts := strings.Split(q.ID.Hex(), "")[len(q.ID.Hex())-10:]
		q.URL += strings.Join(IDParts, "")
	}
}

// Save stores the question to database
func (q *Question) Save() error {
	if len(q.Text) < 1 {
		return errors.New("Question text is empty")
	}
	if len(q.URL) < 1 {
		q.SetText(q.Text)
	}
	if q.ID.Hex() == "" {
		q.ID = bson.NewObjectId()
	}
	return db.Create(ClQuestion, q)
}

// IsValid returns true if all requred fields are not empty
func (q Question) IsValid() bool {
	return q.Text != "" && q.Author.UserID.Hex() != "" && q.URL != ""
}

// MissingFields return a list of required but empty fields
func (q Question) MissingFields() []string {
	missing := []string{}
	if q.Text == "" {
		missing = append(missing, []string{"text"}...)
	}
	if q.Author.UserID.Hex() == "" {
		missing = append(missing, []string{"author"}...)
	}
	if q.URL == "" {
		missing = append(missing, []string{"URL"}...)
	}
	return missing
}

// GetQuestionByID finds question by ID
func GetQuestionByID(qID bson.ObjectId) (Question, error) {
	question := Question{}
	err := db.FindOne(ClQuestion, &bson.M{"_id": qID}, &question)
	return question, err
}
