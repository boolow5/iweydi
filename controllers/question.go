package controllers

import (
	"net/http"

	"bitbucket.org/boolow5/iweydi/models"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

// AddQuestion controller stores question to the database
func AddQuestion(c *gin.Context) {
	userID := c.MustGet("id").(bson.ObjectId)
	question := models.Question{}

	err := c.BindJSON(&question)
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	author, err := models.GetUserByID(userID)
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	question.Author = author.Profile
	if !question.IsValid() {
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"msg": "missing required data", "missing": question.MissingFields()})
			return
		}
	}
	err = question.Save()
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"question": question})
}

// GetQuestion finds a question by it's ID
func GetQuestion(c *gin.Context) {
	questionID := bson.ObjectIdHex(c.Param("questionID"))
	question, err := models.GetQuestionByID(questionID)
	if err != nil {
		ErrorResponse(c, http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"question": question})
}
