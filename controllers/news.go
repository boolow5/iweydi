package controllers

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/boolow5/iweydi/models"
	"github.com/gin-gonic/gin"
)

// GetLatestNewsItems fetches latest news items from page until the page size limit
func GetLatestNewsItems(c *gin.Context) {
	page, _ := strconv.Atoi(c.Query("page"))
	limit, _ := strconv.Atoi(c.Query("limit"))
	category := c.Query("category")
	counter := models.CountItems(models.CL_NEWS_ITEM, bson.M{})
	if counter < 0 {
		ErrorResponse(c, http.StatusInternalServerError, fmt.Errorf("No items in %s collection", models.CL_NEWS_ITEM))
		return
	}

	if counter == 0 {
		ErrorResponse(c, http.StatusInternalServerError, errors.New("No news items found"))
		return
	}

	if page == 0 {
		page = 1
	}

	if limit == 0 {
		limit = 50
	}

	// if page number is more than max pages available
	if (counter / limit) < page {
		page = 1
	}

	// if the items available are less than to be divided to pages set page to 1
	if counter < limit {
		page = 1
	}

	if category == "" {
		category = "all"
	}

	items, err := models.GetNewsItems(page, limit, category)
	if err != nil {
		ErrorResponse(c, http.StatusInternalServerError, err)
		return
	}

	c.JSON(200, gin.H{
		"total":     counter,
		"page":      page,
		"page_size": limit,
		"items":     items,
	})
}
