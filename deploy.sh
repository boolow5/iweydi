export APP_NAME=iweydi
export PKGNAME=${APP_NAME}.tar.gz

echo "deploying ${APP_NAME} to server"
echo "sending ${PKGNAME}..."

export OWNER=mahdi
export USER=webapps
export SERVER=iweydi.com
export DEPLOY_PATH=/webapps/iweydi

if [ ! -f $PKGNAME ]; then
  echo "No package found. Run make package"
  exit 0;
fi

echo "Sending files to server ${SERVER}"
echo "$OWNER@$SERVER"
scp $PKGNAME $OWNER@$SERVER:/home/$OWNER/

ssh -t $OWNER@$SERVER "
  # sudo rm -r $DEPLOY_PATH/api/bin &&
  sudo tar -xvzf /home/$OWNER/$PKGNAME -C $DEPLOY_PATH/api/;
  sudo find $DEPLOY_PATH/api -type d -exec chmod 755 {} \;
  sudo chown -R $OWNER:$USER $DEPLOY_PATH/api;
  sudo supervisorctl restart $APP_NAME || true
";

echo "Deleting ${PKGNAME}..."
rm $PKGNAME
echo "DONE!"
