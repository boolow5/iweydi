package main

import (
	"bitbucket.org/boolow5/iweydi/config"
	"bitbucket.org/boolow5/iweydi/controllers"
	"bitbucket.org/boolow5/iweydi/db"
	"bitbucket.org/boolow5/iweydi/routers"
)

func main() {
	db.Init()
	// go models.Init()
	controllers.InitSocialAuth()
	conf := config.Get()
	routers.Init().Run(conf.Port)
}
